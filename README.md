# Plim

## PHP - less is more (plim). 

- The challenge of smaller project startup PH:P
- using Composer to become great

## Install

Type in a terminal in the folder of your PHP project (Linux, Windows or Mac):

    Composer create-project neos/plim ./ dev-master

Required: [Composer](https://getcomposer.org/download/) & [Git](http://git-scm.com/book/en/Getting-Started-Installing-Git).

## License

This project is licensed under a Creative Commons.

For more information, please see http://creativecommons.org/publicdomain/zero/1.0/

 


